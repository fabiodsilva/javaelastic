/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.javaelastic.singleton;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.util.Base64;
import javax.net.ssl.SSLContext;
import org.apache.http.Header;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.nio.client.HttpAsyncClientBuilder;
import org.apache.http.message.BasicHeader;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.ssl.SSLContexts;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestClientBuilder.HttpClientConfigCallback;
import org.elasticsearch.client.RestHighLevelClient;

/**
 *
 * @author fabio
 */
public final class ConexaoElastic {

    private static RestHighLevelClient INSTANCE;

    public static synchronized RestHighLevelClient getInstance() throws CertificateException, IOException, KeyStoreException, NoSuchAlgorithmException, KeyManagementException {

//                .setHttpClientConfigCallback(new HttpClientConfigCallback() {
//                    @Override
//                    public HttpAsyncClientBuilder customizeHttpClient(
//                            HttpAsyncClientBuilder httpClientBuilder) {
////                        return httpClientBuilder
////                                .setDefaultCredentialsProvider(credentialsProvider);
////                        return httpClientBuilder
////                                .setDefaultCredentialsProvider(credentialsProvider).setSSLContext(sslContext);
//                            return httpClientBuilder
//                                .setDefaultHeaders(defaultHeaders).setSSLContext(sslContext);
//                    }
//
////            @Override
////            public HttpAsyncClientBuilder customizeHttpClient(HttpAsyncClientBuilder httpClientBuilder) {
////                return httpClientBuilder.setDefaultCredentialsProvider(credentialsProvider);
////            }
//                });
        if (INSTANCE == null) {
            Path caCertificatePath = Paths.get("f:\\ca.crt");
            CertificateFactory factory
                    = CertificateFactory.getInstance("X.509");
            Certificate trustedCa;
            try (InputStream is = Files.newInputStream(caCertificatePath)) {
                trustedCa = factory.generateCertificate(is);
            }
            KeyStore trustStore = KeyStore.getInstance("pkcs12");
            trustStore.load(null, null);
            trustStore.setCertificateEntry("ca", trustedCa);
            SSLContextBuilder sslContextBuilder = SSLContexts.custom()
                    .loadTrustMaterial(trustStore, null);
            final SSLContext sslContext = sslContextBuilder.build();
//        RestClient.builder(
//                new HttpHost("localhost", 9200, "https"))
//                .setHttpClientConfigCallback(new HttpClientConfigCallback() {
//                    @Override
//                    public HttpAsyncClientBuilder customizeHttpClient(
//                            HttpAsyncClientBuilder httpClientBuilder) {
//                        return httpClientBuilder.setSSLContext(sslContext);
//                    }
//                });
//{
//  "id" : "oCw-83IBVp2GwHZHtg8y",
//  "name" : "api_key_documentos",
//  "api_key" : "WOQyj1SeQNO1cQZVJ-XyfQ"
//}


            String apiKeyId = "oCw-83IBVp2GwHZHtg8y";
            String apiKeySecret = "WOQyj1SeQNO1cQZVJ-XyfQ";
            String apiKeyAuth
                    = Base64.getEncoder().encodeToString(
                            (apiKeyId + ":" + apiKeySecret)
                                    .getBytes(StandardCharsets.UTF_8));

            Header[] defaultHeaders
                    = new Header[]{new BasicHeader("Authorization",
                                "ApiKey " + apiKeyAuth)};
//builder.setDefaultHeaders(defaultHeaders);

//        final CredentialsProvider credentialsProvider
//                = new BasicCredentialsProvider();
//        credentialsProvider.setCredentials(AuthScope.ANY,
//                new UsernamePasswordCredentials("svc_banco_documentos", "ErG20Hy6"));
//        RestClientBuilder builder = RestClient.builder(
//                new HttpHost("srvkb01hml-trf1.trf1.gov.br", 9200, "https"));
            RestClientBuilder builder = RestClient.builder(
                    new HttpHost("srvkb01hml-trf1.trf1.gov.br", 9200, "https")).setDefaultHeaders(defaultHeaders)
                    .setHttpClientConfigCallback(new HttpClientConfigCallback() {
                        @Override
                        public HttpAsyncClientBuilder customizeHttpClient(
                                HttpAsyncClientBuilder httpClientBuilder) {
                            return httpClientBuilder.setSSLContext(sslContext);
                        }
                    });
            INSTANCE = new RestHighLevelClient(builder);
//            INSTANCE = new RestHighLevelClient(
//                    RestClient.builder(
//                            new HttpHost("srvkb01hml-trf1.trf1.gov.br", 9200, "http"),
//                            new HttpHost("srvkb01hml-trf1.trf1.gov.br", 9201, "http")));
        }
        return INSTANCE;
    }
}
