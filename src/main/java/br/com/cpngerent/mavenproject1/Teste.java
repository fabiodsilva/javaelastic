/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cpngerent.mavenproject1;

import br.com.javaelastic.singleton.ConexaoElastic;
import static java.lang.System.exit;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.http.HttpHost;
import org.elasticsearch.action.DocWriteResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.support.replication.ReplicationResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.elasticsearch.common.xcontent.XContentType;

/**
 *
 * @author Raquel
 */
public class Teste {

    public static void main(String args[]) {
        try {
              RestHighLevelClient client=ConexaoElastic.getInstance();
//            RestHighLevelClient client = new RestHighLevelClient(
//                    RestClient.builder(
//                            new HttpHost("localhost", 9200, "http"),
//                            new HttpHost("localhost", 9201, "http")));

            XContentBuilder builder = XContentFactory.jsonBuilder();
            builder.startObject();
            {
                builder.startObject("processo");
                {
                    builder.startObject("dados");
                    {

                        builder.field("texto", "moço que o sorriso de criança volte sempre me abraçar.");
                        builder.field("numProcesso", "9999999997798888");
                        builder.timeField("dataDistribuicao", new Date());
                        

                    }
                    builder.endObject();
                    builder.startArray("partes");
                    {
                        builder.startObject();
                        {
                            builder.field("tipoParte", "REU");
                            builder.field("nome", "ALINE DAMASCENO");
                        }
                        builder.endObject();
                        builder.startObject();
                        {
                            builder.field("tipoParte", "AUTOR");
                            builder.field("nome", "RAQUEL");
                        }
                        builder.endObject();

                    }
                    builder.endArray();

                }
                builder.endObject();

            }
            builder.endObject();
//            IndexRequest request = new IndexRequest("posts", "doc","7768899-1")
//                    .source(builder);
            IndexRequest request = new IndexRequest("posts2")
            .id("85726483432").source(builder);
            IndexResponse indexResponse = client.index(request,RequestOptions.DEFAULT);
            String index = indexResponse.getIndex();
            
            String id = indexResponse.getId();
            long version = indexResponse.getVersion();
            if (indexResponse.getResult() == DocWriteResponse.Result.CREATED) {
                System.out.println("CRIADO");
            } else if (indexResponse.getResult() == DocWriteResponse.Result.UPDATED) {
                System.out.println("ATULIZADO");
            }
            ReplicationResponse.ShardInfo shardInfo = indexResponse.getShardInfo();
            if (shardInfo.getTotal() != shardInfo.getSuccessful()) {

            }
            if (shardInfo.getFailed() > 0) {
                for (ReplicationResponse.ShardInfo.Failure failure : shardInfo.getFailures()) {
                    String reason = failure.reason();
                }
            }
            exit(0);
//            TransportClient client = new PreBuiltTransportClient(Settings.EMPTY)
//        .addTransportAddress(new TransportAddress(InetAddress.getByName("host1"), 9300))
//        .addTransportAddress(new TransportAddress(InetAddress.getByName("host2"), 9300));

//            client.close();
//            IndexRequest request = new IndexRequest("posts");
//            request.id("1");
//            String jsonString = "{"
//                    + "\"user\":\"kimchy\","
//                    + "\"postDate\":\"2013-01-30\","
//                    + "\"message\":\"trying out Elasticsearch\""
//                    + "}";
//            request.source(jsonString, XContentType.JSON);
//IndexRequest request = new IndexRequest("twitter"); 
////request.id("1"); 
//String jsonString = "{" +
//        "\"user\":\"kimchy\"," +
//        "\"postDate\":\"2013-01-30\"," +
//        "\"message\":\"trying out Elasticsearch\"" +
//        "}";
//request.source(jsonString, XContentType.JSON);
//IndexResponse indexResponse = client.index(request, RequestOptions.DEFAULT);
//String index = indexResponse.getIndex();
//String id = indexResponse.getId();
//if (indexResponse.getResult() == DocWriteResponse.Result.CREATED) {
//    System.out.println("Criado");
//} else if (indexResponse.getResult() == DocWriteResponse.Result.UPDATED) {
//    System.out.println("Atualizado");
//}
//ReplicationResponse.ShardInfo shardInfo = indexResponse.getShardInfo();
//if (shardInfo.getTotal() != shardInfo.getSuccessful()) {
//    System.out.println("shard diferente");
//}
//if (shardInfo.getFailed() > 0) {
//    for (ReplicationResponse.ShardInfo.Failure failure :
//            shardInfo.getFailures()) {
//        String reason = failure.reason();
//        System.out.println(reason);
//    }
//}
//CreateIndexRequest request = new CreateIndexRequest("twitter");
//
//CreateIndexResponse createIndexResponse = client.indices().create(request, RequestOptions.DEFAULT);
//OpenIndexRequest request = new OpenIndexRequest("produtos");
//OpenIndexResponse openIndexResponse = client.indices().open(request, RequestOptions.DEFAULT);
//CloseIndexRequest request1 = new CloseIndexRequest("produtos");
//AcknowledgedResponse closeIndexResponse = client.indices().close(request1, RequestOptions.DEFAULT);
//boolean abriu=client.indices().open(new OpenIndexRequest("produtos"), RequestOptions.DEFAULT).isAcknowledged();
//System.out.println("Abriu: "+abriu);
//IndexResponse response = client.prepareIndex("twitter", "_doc", "1")
//        .setSource(jsonBuilder()
//                    .startObject()
//                        .field("user", "kimchy")
//                        .field("postDate", new Date())
//                        .field("message", "trying out Elasticsearch")
//                    .endObject()
//                  )
//        .get();
//boolean fechou=client.indices().close(new CloseIndexRequest("customer"), RequestOptions.DEFAULT).isAcknowledged();
//
//
//            
//            System.out.println("Fechou: "+fechou);
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("Erro");
            Logger.getLogger(Teste.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
