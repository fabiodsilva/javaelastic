/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cpngerent.mavenproject1;

import br.com.javaelastic.singleton.ConexaoElastic;
import java.io.IOException;
import static java.lang.System.exit;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.http.HttpHost;
import org.apache.lucene.search.join.ScoreMode;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.text.Text;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.InnerHitBuilder;
import org.elasticsearch.index.query.NestedQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import static org.elasticsearch.index.query.QueryBuilders.boolQuery;
import static org.elasticsearch.index.query.QueryBuilders.matchPhraseQuery;
import static org.elasticsearch.index.query.QueryBuilders.matchQuery;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.elasticsearch.index.query.QueryBuilders.simpleQueryStringQuery;
import static org.elasticsearch.index.query.QueryBuilders.termQuery;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;
import org.elasticsearch.search.sort.ScoreSortBuilder;
import org.elasticsearch.search.sort.SortOrder;
//import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 *
 * @author Raquel
 */
public class ConsultaElastic {

    public static void main(String args[]) {

        try {
            RestHighLevelClient client = ConexaoElastic.getInstance();
//            RestHighLevelClient client = new RestHighLevelClient(
//                    RestClient.builder(
//                            new HttpHost("localhost", 9200, "http"),
//                            new HttpHost("localhost", 9201, "http")));

//        searchRequest.types("doc");
//        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
//        searchSourceBuilder.query(QueryBuilders.matchAllQuery());
//        searchRequest.source(searchSourceBuilder);
//        QueryBuilder matchQueryBuilder = QueryBuilders.matchQuery("user", "kimchy")
//                                                .fuzziness(Fuzziness.AUTO)
//                                                .prefixLength(3)
//                                                .maxExpansions(10);
//        searchSourceBuilder.query(matchQueryBuilder);


            SearchRequest searchRequest = new SearchRequest();
            searchRequest.indices("legado_v2");
            SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
            String livre = "Direito civil";
            String frase = "\"Direito civil\"  ";//ter o lembrando são coisinhas e não ter o muito
            BoolQueryBuilder b = boolQuery()
                    //        .must(termQuery("processo.dados.numProcesso", "7677334888999959"))                 
                    //        .must(matchPhraseQuery("processo.dados.texto", "um trecho jhghjghjgjhg"))
                    //        .must(matchQuery("processo.dados.texto", livre,
//                    .must(QueryBuilders.nestedQuery("processo.partes",
//                    QueryBuilders.boolQuery()
//                            .must(QueryBuilders.matchQuery("processo.partes.nome", "INSS"))
//                            .must(QueryBuilders.matchQuery("processo.partes.tipoParte", "REU")),
//                     ScoreMode.None))
                    .must(queryStringQuery(frase).field("processo.documentos.conteudoDocumento"));
            //          .must(simpleQueryStringQuery(frase));

            //          .must(matchPhraseQuery("processo.dados.texto", livre))
            //        .mustNot(termQuery("content", "test2"))              
            //        .should(termQuery("content", "test3"))               
            //        .filter(termQuery("content", "test5"))
            ;
            if (false) {
                b.must(termQuery("processo.dados.numProcesso", "7777334888999959"));
            }
            //sourceBuilder.query(QueryBuilders.nestedQuery("processo.partes", QueryBuilders.boolQuery().must(QueryBuilders.queryStringQuery("RAQUEL").field("processo.partes.nome")), ScoreMode.None));

//            SearchResponse response = client().prepareSearch("test")
//                    .setQuery(boolQuery().must(matchQuery("key", "value"))
//                            .should(hasChildQuery("child_type", nestedQuery("nested_type", matchAllQuery(), ScoreMode.None)
//                                    .innerHit(new InnerHitBuilder(), false), ScoreMode.None).innerHit(new InnerHitBuilder(), false)))
//                    .get();
//            sourceBuilder.query(QueryBuilders.nestedQuery("processo.partes",
//                    QueryBuilders.boolQuery()
//                            .must(QueryBuilders.matchQuery("processo.partes.nome", "RAQUEL"))
//                            .must(QueryBuilders.matchQuery("processo.partes.tipoParte", "REU")),
//                     ScoreMode.None));
            sourceBuilder.query(b);
            HighlightBuilder highlightBuilder = new HighlightBuilder();
            HighlightBuilder.Field highlightTexto = new HighlightBuilder.Field("processo.documentos.conteudoDocumento");
            highlightBuilder.field(highlightTexto);
            sourceBuilder.highlighter(highlightBuilder);
//sourceBuilder.query(QueryBuilders.termQuery("processo.dados.texto", "um trecho")); 
            sourceBuilder.from(0);
            sourceBuilder.size(10);//seta a quantidade de resultados
            sourceBuilder.timeout(new TimeValue(60, TimeUnit.SECONDS));
            sourceBuilder.sort(new ScoreSortBuilder().order(SortOrder.DESC));
            searchRequest.source(sourceBuilder);
            SearchResponse searchResponse = client.search(searchRequest, RequestOptions.DEFAULT);
            SearchHits hits = searchResponse.getHits();
            SearchHit[] searchHits = hits.getHits();
            System.out.println(hits.getTotalHits());
            for (SearchHit hit : searchHits) {

                String sourceAsString = hit.getSourceAsString();
                Map<String, Object> sourceAsMap = hit.getSourceAsMap();
                Map<String, Object> processo = (Map<String, Object>) sourceAsMap.get("processo");
                Map<String, Object> dados = (Map<String, Object>) processo.get("dados");
                String numProcesso = (String) dados.get("numProcesso");

                //String documentTitle = (String) sourceAsMap.get("processo.dados.numProcesso");
                System.out.println(numProcesso);
                List<Object> partes = (List<Object>) processo.get("partes");

                Map<String, HighlightField> highlightFields = hit.getHighlightFields();
                HighlightField highlight = highlightFields.get("processo.documentos.conteudoDocumento");
                Text[] fragments = highlight.fragments();
                String fragmentString = fragments[0].string();
                System.out.println(fragmentString);
//Map<String, Object> innerObject =
//        (Map<String, Object>) sourceAsMap.get("innerObject");
                // do something with the SearchHit
            }
            client.close();
            exit(0);

//        SearchResponse response = client.prepareSearch("index1", "index2")
//        .setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
//        .setQuery(QueryBuilders.termQuery("multi", "test"))                 // Query
//        .setPostFilter(QueryBuilders.rangeQuery("age").from(12).to(18))     // Filter
//        .setFrom(0).setSize(60).setExplain(true)
//        .get();
        } catch (Exception ex) {
            ex.printStackTrace();
            Logger.getLogger(ConsultaElastic.class.getName()).log(Level.SEVERE, null, ex);
            exit(0);
        }
    }

}
